#/bin/bash
#
# Rename the files in all directories in the current path to the name of the
# directory.

set -o errexit
set -o nounset
set -o pipefail

# Return the filename extension of the provided file.
get_extension() {
  if [[ "$1" = "${1#*.}" ]]; then
    echo ""
  else
    echo ".${1#*.}"
  fi
}

# Return true if a file following the given pattern exists, else false.
file_exists() {
  for item in $1; do
    if [[ -e "$item" ]]; then
      return 0
    else
      return 1
    fi
    break
  done
}

# Incrementally rename (i.e. name_1, name_2, ..., name_n) all files, in the
# provided directory, that matches the pattern.
incremental_rename() {
  dir="$1"
  files=$( find ./* -type f )
  i=1
  for file in $files; do
    filename=$( basename "$file" )
    # Ignore files that already follow the naming convention.
    if [[ "${filename}" != "${dir}_"* ]]; then
      extension=$( get_extension "${filename}" )
      new_name="${dir}_${i}${extension}"
      # Increase index by one if its already used by another file.
      while file_exists "${dir}_${i}"*; do
        i=$(( i += 1 ))
        new_name="${dir}_${i}${extension}"
      done
      mv -v -- "${filename}" "$new_name"
    fi
  done
}

main() {
  dirs=$( find ./* -type d )
  for directory in $dirs; do
    ( cd "$directory" ; incremental_rename "$( basename "$directory" )" )
  done
}

main "$@"
