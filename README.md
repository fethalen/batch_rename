batch_rename
------------

### Introduction

I wrote this script as a solution to the following problem: Given a set of
directories named after different species, rename the files inside that directory to the
name of the directory in an incremental fashion (i.e., file_1, file_2, ...,
file_n).

### Installation

Use git to download the directory containing the script:

    git clone https://github.com/fethalen/batch_rename batch_rename

Make the script executable.

    chmod +x batch_rename.sh

### Usage

Run as such:

    ./batch_rename.sh

### Example

Before:

    Drosophilia_melanogaster/
    ├── poorly_named_file_1.jpg
    └── poorly_named_file_2.jpg

After:

    Drosophilia_melanogaster/
    ├── Drosophila_melanogaster_1.jpg
    └── Drosophila_melanogaster_2.jpg
